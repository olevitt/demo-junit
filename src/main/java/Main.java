import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import preferences.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        PreferencesService prefService = new PreferencesServiceJSON();
        //PreferencesService preferencesService = PreferencesServiceFactory.getPreferencesService();
        ApplicationContext context = new AnnotationConfigApplicationContext(PreferencesServiceJSON.class);
        PreferencesService preferencesService  = context.getBean(PreferencesService.class);
        System.out.println(preferencesService.getClass().getCanonicalName());
        Preferences preferences = preferencesService.load();
        if (preferences != null) {
            System.out.println("Preferences chargées");
        }
        else {
            System.out.println("Pas de préférences");
        }
    }


}
