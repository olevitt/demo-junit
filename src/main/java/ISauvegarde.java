import java.io.IOException;

public interface ISauvegarde {

    public void sauvegarder(int score) throws IOException;
}
