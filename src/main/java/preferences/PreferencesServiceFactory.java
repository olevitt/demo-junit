package preferences;

public class PreferencesServiceFactory {

    public static PreferencesService getPreferencesService() {
        String storageMode = System.getenv("STORAGE_MODE");
        System.out.println("Storage mode : "+storageMode);
        if ("JSON".equals(storageMode)) {
            return new PreferencesServiceJSON();
        }
        else {
            return new PreferencesServiceMemory();
        }
    }
}
