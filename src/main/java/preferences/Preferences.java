package preferences;

import java.util.Set;

public class Preferences {

    private String pseudo;

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPseudo() {
        return pseudo;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Preferences)) {
            return false;
        }

        Preferences preferences = (Preferences) obj;

        return this.getPseudo().equals(preferences.getPseudo());
    }
}
