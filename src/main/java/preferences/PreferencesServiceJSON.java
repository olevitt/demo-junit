package preferences;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Conditional;

import java.io.File;
import java.io.IOException;

public class PreferencesServiceJSON implements PreferencesService {

    private static ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void save(Preferences preferences) {
        try {
            objectMapper.writeValue(new File("preferences.json"),preferences);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Preferences load() {
        try {
            return objectMapper.readValue(new File("preferences.json"),Preferences.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
