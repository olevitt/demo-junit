package preferences;

/**
 * Cette implémentation n'offre pas de persistence
 */
public class PreferencesServiceMemory implements PreferencesService {

    private static Preferences preferences;

    @Override
    public void save(Preferences preferences) {
        PreferencesServiceMemory.preferences = preferences;
    }

    @Override
    public Preferences load() {
        return preferences;
    }
}
