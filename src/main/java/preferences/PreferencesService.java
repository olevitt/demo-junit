package preferences;

public interface PreferencesService {

    /**
     * Sauvegarde les preferences.
     * @param preferences
     */
    public void save(Preferences preferences);

    /**
     *
     * @return les préférences chargées ou null si il n'y en a pas ou si il y a un problème
     */
    public Preferences load();
}
