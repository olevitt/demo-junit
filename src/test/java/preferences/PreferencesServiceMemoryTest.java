package preferences;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PreferencesServiceMemoryTest {

    @Test
    public void shouldReturnNullIfNoStoredPreferences() {
        PreferencesService memory = new PreferencesServiceMemory();
        Preferences preferences = memory.load();
        Assertions.assertEquals(null, preferences);
    }

    @Test
    public void shouldSaveAndLoad() {
        Preferences preferences = new Preferences();
        preferences.setPseudo("toto");
        PreferencesService memory = new PreferencesServiceMemory();

        memory.save(preferences);
        Preferences preferences1 = memory.load();

        Assertions.assertEquals(preferences.getPseudo(), preferences1.getPseudo());
    }
}
