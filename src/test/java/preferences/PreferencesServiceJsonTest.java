package preferences;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

public class PreferencesServiceJsonTest {

    @BeforeEach
    public void menage() {
        File file = new File("preferences.json");
        if (file.exists()) {
            file.delete();
        }
    }

    @Test
    public void shouldReturnNullIfNoStoredPreferences() {
        PreferencesService memory = new PreferencesServiceJSON();
        Preferences preferences = memory.load();
        Assertions.assertEquals(null, preferences);
    }

    @Test
    public void shouldSaveAndLoad() {
        Preferences preferences = new Preferences();
        preferences.setPseudo("toto");
        PreferencesService memory = new PreferencesServiceJSON();

        memory.save(preferences);
        Preferences preferences1 = memory.load();

        Assertions.assertEquals(preferences.getPseudo(), preferences1.getPseudo());
    }
}
