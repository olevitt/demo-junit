package preferences;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class PreferencesTest {

    @Test
    public void shouldBeEqualsIfSameData() {
        Preferences pref1 = new Preferences();
        pref1.setPseudo("tata");
        Preferences pref2 = new Preferences();
        pref2.setPseudo("tata");
        Assertions.assertTrue(pref1.equals(pref2));
    }
}
