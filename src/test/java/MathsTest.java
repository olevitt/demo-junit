import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.annotation.Testable;

public class MathsTest {

    @Test
    public void testAddition() {
        // GIVEN | Arrange
        Maths maths = new Maths();

        // WHEN | Act
        int resultat = maths.addition(2,3);

        // THEN | Assert
        Assertions.assertEquals(6, resultat);
    }
}
